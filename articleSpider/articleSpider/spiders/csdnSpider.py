# -*- coding: utf-8 -*-
import scrapy
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import CrawlSpider, Rule
from lxml import etree
from ..items import ArticlespiderItem
from ..utils import *
import re

class CsdnspiderSpider(CrawlSpider):
    name = 'csdnSpider'
    allowed_domains = ['csdn.net']
    start_urls = [
                'https://so.csdn.net/so/search/s.do?p=20&q=python',  # python
                'https://so.csdn.net/so/search/s.do?q=爬虫',  # 爬虫
                'https://so.csdn.net/so/search/s.do?q=python+爬虫',
                'https://so.csdn.net/so/search/s.do?q=python',
                'https://so.csdn.net/so/search/s.do?q=scrapy',  # scrapy框架
                'https://so.csdn.net/so/search/s.do?q=redis+scrapy', # 分布式
                'https://so.csdn.net/so/search/s.do?q=django', # django
                ]
    """
    博客
    https://blog.csdn.net/chuhe163/article/details/79006793
    专栏
    https://blog.csdn.net/lanphaday/column/info/python-can
    图文课：
    https://gitchat.csdn.net/column/5b4445fb88bf540ab6007339
    """
    rules = (
        # 页码以及专栏
        # # https://so.csdn.net/so/search/s.do?p=3&q=python
        Rule(LinkExtractor(allow=r'\?p='), callback='parse_page', follow=True),
        Rule(LinkExtractor(allow=r'/column/info/'), callback='parse_page', follow=True),
        # 详情页
        Rule(LinkExtractor(allow=r'article/details/'), callback='parse_item', follow=False),
    )

    def parse_page(self, response):
        links = response.xpath(r"//dl[@class='search-list J_search']/dt/div[@class='limit_width']/a/@href").extract()
        print("当前解析出链接：%d" % (len(links)))
        for link in links:
            scrapy.Request(link)

    def parse_item(self, response):
        i = ArticlespiderItem()

        # 博客标题
        title = response.xpath(r"//div[@class='article-title-box']/h1[@class='title-article']/text()").extract()[0]
        # 发布时间
        publishTime = response.xpath(
            r"//div[@class='article-info-box']/div[@class='article-bar-top']/span[@class='time']/text()").extract()[0]
        # 作者
        author = response.xpath(
            r"//div[@class='article-info-box']/div[@class='article-bar-top']/a[@class='follow-nickName']/text()").extract()[0]
        # 来源
        source = "CSDN"
        # 阅读数
        readNum = response.xpath(
            r"//div[@class='article-header']/div[@class='article-info-box']/div[@class='article-bar-top']/span[@class='read-count']/text()").extract()[0][4:]

        # 内容 HTML格式
        html = response.text
        ehtml = etree.HTML(html)
        contenttag = ehtml.xpath('//article')[0]
        contents = etree.tostring(contenttag, encoding='utf-8',method='html').decode('utf-8')  # 转为字符串
        contents = contents.replace('"',r'\"')
        i['title'] = title
        i['publishTime'] = publishTime
        i['author'] = author
        i['source'] = source
        i['readNum'] = readNum
        i['url'] = response.url
        i['contents'] = contents
        return i
