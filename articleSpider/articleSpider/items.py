# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class ArticlespiderItem(scrapy.Item):
    # define the fields for your item here like:
    # 博客标题
    title = scrapy.Field()
    url = scrapy.Field()
    # 发布时间
    publishTime = scrapy.Field()
    # 作者
    author = scrapy.Field()
    # 来源
    source = scrapy.Field()
    # 阅读数
    readNum = scrapy.Field()
    # 内容 HTML格式
    contents = scrapy.Field()



