import re


def dealTime(datastr):
    """
    处理时间格式：2017年11月10日 12:29:47 =》2017-11-10 12:29:47
    :param datastr:
    :return: time
    """
    # time = re.sub(r"[\u4e00-\u9fa5]","-", datastr, count=0, flags=0)  # 将汉字替换成’-'
    datastr = datastr.replace('年', '-')
    datastr = datastr.replace('月', '-')
    datastr = datastr.replace('日', '')
    return datastr


def dealDoubleQuotation(data):
    return data.replace("\"", "@jk_DQ")


def unDealDoubleQuotation(data):
    return data.replace("@jk_DQ", "\"")


if __name__ == '__main__':
    ss = """
    <div class="blog-content-box">
	<div class="article-header-box">
		<div class="article-header">
			<div class="article-title-box">
				<span class="article-type type-2 float-left">转</span>				<h1 class="title-article">python下载版本区别</h1>
			</div>
			<div class="article-info-box">
				<div class="article-bar-top">
																				<span class="time">2018年01月08日 20:24:41</span>
					<a class="follow-nickName" href="https://me.csdn.net/chuhe163" target="_blank">谁动了我的二叉树</a>
						<span class="read-count">阅读数：14935</span>
										</div>
				<div class="operating">
									</div>
			</div>
		</div>
	</div>
	<article class="baidu_pl">
		<div id="article_content" class="article_content clearfix csdn-tracking-statistics" data-pid="blog" data-mod="popu_307" data-dsm="post">
								            <link rel="stylesheet" href="https://csdnimg.cn/release/phoenix/template/css/ck_htmledit_views-f57960eb32.css">
						<div class="htmledit_views" id="content_views">
                
<span style="color:rgb(69,69,69);font-family:'PingFang SC', 'Microsoft YaHei', SimHei, Arial, SimSun;line-height:24px;font-size:18px;"><strong>python官网有几个下载文件，有什么区别？</strong></span><br style="color:rgb(69,69,69);font-family:'PingFang SC', 'Microsoft YaHei', SimHei, Arial, SimSun;line-height:24px;"><br style="color:rgb(69,69,69);font-family:'PingFang SC', 'Microsoft YaHei', SimHei, Arial, SimSun;line-height:24px;"><span style="color:rgb(69,69,69);font-family:'PingFang SC', 'Microsoft YaHei', SimHei, Arial, SimSun;line-height:24px;font-size:14px;">Python 3.6.0a1 - 2016-05-17<br>
Download Windows x86 web-based installer<br>
Download Windows x86 executable installer<br>
Download Windows x86 embeddable zip file<br>
Download Windows x86-64 web-based installer<br>
Download Windows x86-64 executable installer<br>
Download Windows x86-64 embeddable zip file</span><span style="color:rgb(69,69,69);font-family:'PingFang SC', 'Microsoft YaHei', SimHei, Arial, SimSun;line-height:24px;"></span>
<p style="color:rgb(69,69,69);font-family:'PingFang SC', 'Microsoft YaHei', SimHei, Arial, SimSun;line-height:24px;">
<span style="font-size:14px;"><br></span></p>
<p style="color:rgb(69,69,69);font-family:'PingFang SC', 'Microsoft YaHei', SimHei, Arial, SimSun;line-height:24px;">
<span style="font-size:14px;">x86是32位，x86-64是<a href="https://www.baidu.com/s?wd=64%E4%BD%8D&amp;tn=24004469_oem_dg&amp;rsv_dl=gh_pl_sl_csd" target="_blank">64位</a>。</span></p>
<p style="color:rgb(69,69,69);font-family:'PingFang SC', 'Microsoft YaHei', SimHei, Arial, SimSun;line-height:24px;">
<span style="font-size:14px;">可以通过下面3种途径获取python：</span></p>
<p style="color:rgb(69,69,69);font-family:'PingFang SC', 'Microsoft YaHei', SimHei, Arial, SimSun;line-height:24px;">
<span style="font-size:14px;">web-based installer 是需要通过联网完成安装的<br></span><span style="font-size:14px;">executable installer 是可执行文件(*.exe)方式安装<br></span><span style="font-size:14px;">embeddable zip file 嵌入式版本，可以集成到其它应用中。</span></p>
            </div>
                </div>
	</article>
</div>
    """
    da = dealDoubleQuotation(ss)

    print(unDealDoubleQuotation(da))