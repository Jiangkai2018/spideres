# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html

import pymysql
from .utils import *
from scrapy.conf import settings


class ArticlespiderPipeline(object):

    def __init__(self):

        config = {
            "host": settings["MYSQL_HOST"],
            "user": settings["MYSQL_USER"],
            "port": settings["MYSQL_PORT"],
            "password": settings["MYSQL_PASSWORD"],
            "database": settings["MYSQL_DB"]
        }
        self.db = pymysql.connect(**config)
        self.cursor = self.db.cursor()
        # test connect
        '''
        cursor.execute("SELECT VERSION()")
        data = cursor.fetchone()
        print("Database version : %s " % data)
        '''

    def process_item(self, item, spider):
        item = dict(item)
        title = item['title']
        publishTime = dealTime(item['publishTime'])
        author = item['author']
        source = item['source']
        readNum = item['readNum']
        url = item['url']
        contents = item['contents']
        # filename = "./articleshtml/"+re.findall("/(\d+)$", url, flags=0)[0]
        # with open(filename) as f:
        #     contents = f.read()
        insert_sql = """
        insert into articles(title,publishTime,author,source,readNum,contents,url)
        VALUES("{}","{}","{}","{}",{},"{}","{}");
        """.format(title,publishTime,author,source,readNum,contents,url)
        print("insert_sql======",insert_sql)
        self.cursor.execute(insert_sql)
        self.db.commit()
        return item

