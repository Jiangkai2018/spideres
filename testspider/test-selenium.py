#!coding=utf-8
from selenium import webdriver

chromedriver = r"C:\Users\16372\AppData\Local\Google\Chrome\Application\chromedriver.exe"



#!/usr/bin/env python
# -*- coding:utf-8 -*-

import unittest
from selenium import webdriver
from bs4 import BeautifulSoup as bs


    # 初始化方法，必须是setUp()

driver = webdriver.Chrome(chromedriver)
num = 0
count = 0

    # 测试方法必须有test字样开头

driver.get("https://www.douyu.com/directory/all")

while True:
    soup = bs(driver.page_source, "lxml")
    # 房间名, 返回列表
    names = soup.find_all("h3", {"class" : "ellipsis"})
    print (names)
    # 观众人数, 返回列表
    numbers = soup.find_all("span", {"class" :"dy-num fr"})

    # zip(names, numbers) 将name和number这两个列表合并为一个元组 : [(1, 2), (3, 4)...]
    for name, number in zip(names, numbers):
        # print u"观众人数: -" + number.get_text().strip() + u"-\t房间名: " + name.get_text().strip()
        num += 1
        #self.count += int(number.get_text().strip())

    # 如果在页面源码里找到"下一页"为隐藏的标签，就退出循环
    if driver.page_source.find("shark-pager-disable-next") != -1:
            break

    # 一直点击下一页
    driver.find_element_by_class_name("shark-pager-next").click()

# 测试结束执行的方法

# Firefox()浏览器
print("当前网站直播人数" + str(num))
print("当前网站观众人数" + str(count))
driver.quit()

