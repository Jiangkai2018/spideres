from lxml import etree
text = '''
<div>
    <ul>
         <li class="item-0"><a href="link1.html">知识 item</a></li>
         <li class="item-1"><a href="link2.html">second item</a></li>
         <li class="item-inactive"><a href="link3.html">third item</a></li>
         <li class="item-1"><a href="link4.html">fourth item</a></li>
         <li class="item-0"><a href="link5.html">fifth item</a>
     </ul>
 </div>
'''
ehtml = etree.HTML(text)
html = ehtml.xpath('//li[@class="item-0"]')[0]
# result = etree.tostring(html).decode('utf-8')
result = etree.tostring(html, encoding='utf-8',method='html').decode('utf-8')
print(type(result))