from urllib import request
import urllib
baiduURL = "http://www.baidu.com/"
"""
res = request.urlopen(baiduURL,data=None,timeout=10)
page= res.read()
print(page)
"""
# 带UA
# ua_header = {"User-Agent": "Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/5.0;"}
# requestobj = request.Request(url=baiduURL, headers=ua_header, method="GET")
# requestobj.add_header("Connection", "keep-alive")
# response = request.urlopen(requestobj)
# content = response.read().decode("utf-8")
# print(content)

#
# formdata = {
#     'start':'0',
#     'limit':'10'
# }
# data = urllib.parse.urlencode(formdata)
# print(data)
#

#
# import urllib.request
# import ssl
#
# ssl._create_default_https_context = ssl._create_unverified_context
# response = urllib.request.urlopen('https://www.python.org')
# print(response.read().decode('utf-8'))


# 设置代理，cookies 的opener
# urllib2 = urllib.request
# handler = urllib2.HTTPHandler()
# opener = urllib2.build_opener(handler)
# request = urllib2.Request('http://www.baidu.com/')
# resposne = opener.open(request)
# print(resposne.read().decode('utf-8'))

#proxyhandler = urllib.request.ProxyHandler({"http":"124.88.67.81:80"})
# nullproxy_handler  = urllib.request.ProxyHandler({})
#
# opener = urllib.request.build_opener(proxyhandler)
# request = urllib.request.Request("http://www.baidu.com/")
# print(opener.open(request).read().decode("utf-8"))



"""
from http import cookiejar
urllib2 = urllib.request

# 保存cookie的本地磁盘文件名
filename = 'cookie.txt'

# 声明一个MozillaCookieJar(有save实现)对象实例来保存cookie，之后写入文件
cookiejar = cookiejar.MozillaCookieJar(filename)

# 使用HTTPCookieProcessor()来创建cookie处理器对象，参数为CookieJar()对象
handler = urllib2.HTTPCookieProcessor(cookiejar)

# 通过 build_opener() 来构建opener
opener = urllib2.build_opener(handler)

# 创建一个请求，原理同urllib2的urlopen
response = opener.open("http://www.baidu.com")

# 保存cookie到本地文件
cookiejar.save()

"""
from http import cookiejar
cookielib = cookiejar
urllib2 = urllib.request
"""
# 创建MozillaCookieJar(有load实现)实例对象
cookiejar = cookielib.MozillaCookieJar()

# 从文件中读取cookie内容到变量
cookiejar.load('cookie.txt')

# 使用HTTPCookieProcessor()来创建cookie处理器对象，参数为CookieJar()对象
handler = urllib2.HTTPCookieProcessor(cookiejar)

# 通过 build_opener() 来构建opener
opener = urllib2.build_opener(handler)
response = opener.open("http://www.baidu.com")
"""


